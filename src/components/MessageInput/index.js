import React from 'react';
import styled from 'styled-components';
import { TextField } from '@material-ui/core';

const StyledInput = styled(TextField)`
  
`;

const MessageInput = () => {
    return (
        <StyledInput></StyledInput>
    );
}

export default MessageInput;