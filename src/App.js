import React, { Component } from 'react';
import styled from 'styled-components';
import {Typography, Grid} from '@material-ui/core'
import MessageInput from './components/MessageInput'

const AppTitle = styled(Typography)`
  
`;

class App extends Component {
  render() {
    return (
      <Grid container justify="center">
        <Grid container item xs={6}s>
        <AppTitle variant="headline">Hello</AppTitle>
        <MessageInput></MessageInput>
        </Grid>
      </Grid>
    );
  }
}

export default App;
